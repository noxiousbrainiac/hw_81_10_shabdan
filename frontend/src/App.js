import React from 'react';
import LinkCreatorContainer from "./containers/LinkCreatorContainer/LinkCreatorContainer";

const App = () => {
    return (
        <>
            <LinkCreatorContainer/>
        </>
    );
};

export default App;