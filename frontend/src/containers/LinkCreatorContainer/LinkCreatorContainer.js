import React, {useState} from 'react';
import {Button, CircularProgress, Container, makeStyles, TextField} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {postLink} from "../../store/actions";

const useStyles = makeStyles({
    mainBlock: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "space-between",
        paddingTop: "100px"
    },
    getLink: {
        textAlign: "center"
    },
    input: {
        marginBottom: "20px",
        width: "500px"
    },
    preloaderContainer: {
        position: "relative",
    },
    preloader: {
        position: "absolute",
        left: "50%",
        translate: "transformX(-50%)",
        marginTop: "300px"
    }
});

const LinkCreatorContainer = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const shortUrl = useSelector(state => state.shortUrl);
    const loading = useSelector(state => state.loading);

    const [input, setInput] = useState({
        originalUrl: ""
    });

    const inputHandler = (e) => {
        const {name, value} = e.target;
        setInput(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    const post = async (e) => {
        e.preventDefault();
        await dispatch(postLink(input));
    }

    let linkApp = (
        <form
            className={classes.mainBlock}
            onSubmit={e => post(e)}
        >
            <h2>Shorten your link!</h2>
            <TextField
                className={classes.input}
                onChange={e => inputHandler(e)}
                value={input.originalUrl}
                name="originalUrl"
                size="small"
                variant="outlined"
            />
            <Button
                variant="contained"
                color="primary"
                type="submit"
            >
                Shorten!
            </Button>
            {
                shortUrl
                    ?
                    <div className={classes.getLink}>
                        <h4>Your link now look like this:</h4>
                        <a
                            href={`http://localhost:8000/links/${shortUrl}`}
                            target="_blank"
                            rel="noreferrer"
                        >
                            {`http://localhost:8000/links/${shortUrl}`}
                        </a>
                    </div>
                    :
                    null
            }
        </form>
    );

    if (loading === true) {
        linkApp = (
            <div className={classes.preloaderContainer}>
                <CircularProgress className={classes.preloader}/>
            </div>
        );
    }

    return (
        <Container>
            {linkApp}
        </Container>
    );
};

export default LinkCreatorContainer;