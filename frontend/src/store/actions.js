import axios from "axios";

export const POST_LINK_REQUEST = "POST_LINK_REQUEST";
export const POST_LINK_SUCCESS = "POST_LINK_SUCCESS";
export const POST_LINK_FAILURE = "POST_LINK_FAILURE";

export const postLinkRequest = () => ({type: POST_LINK_REQUEST});
export const postLinkSuccess = (shorUrl) => ({type: POST_LINK_SUCCESS, payload: shorUrl});
export const postLinkFailure = (error) => ({type: POST_LINK_FAILURE, payload: error});

export const postLink = (link) => async (dispatch) => {
    try {
        dispatch(postLinkRequest());
        const {data} = await axios.post('http://localhost:8000/links', link);
        dispatch(postLinkSuccess(data.shortUrl));
    } catch (e) {
        dispatch(postLinkFailure(e));
    }
}