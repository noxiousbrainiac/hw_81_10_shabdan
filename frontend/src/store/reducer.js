import {POST_LINK_FAILURE, POST_LINK_REQUEST, POST_LINK_SUCCESS} from "./actions";

const initialState = {
    shortUrl: "",
    error: null,
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_LINK_REQUEST:
            return {...state, loading: true};
        case POST_LINK_SUCCESS:
            return {...state, shortUrl: action.payload, loading: false};
        case POST_LINK_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
};

export default reducer;