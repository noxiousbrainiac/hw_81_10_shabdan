const express = require('express');
const Links = require('../models/Links');
const {nanoid} = require('nanoid');

const router = express.Router();

router.get('/:shortUrl', async (req, res) => {
    try {
        const findUrl = await Links.findOne({shortUrl: req.params.shortUrl});

        if (findUrl !== null) {
            return res.status(301).redirect(findUrl.originalUrl);
        }
        console.log(findUrl);

        res.status(404).send({error: "Not found"});
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/', async (req, res) => {
    if (!req.body.originalUrl) {
        return res.status(400).send({error: "Data not valid"});
    }

    const link = {
        shortUrl: nanoid(7),
        originalUrl: req.body.originalUrl
    }

    const newLink = new Links(link);

    try {
        await newLink.save();
        res.send(newLink);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;