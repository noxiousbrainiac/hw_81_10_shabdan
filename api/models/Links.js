const mongoose = require('mongoose');

const LinksSchema = new mongoose.Schema({
    originalUrl: {
        type: String,
        required: true
    },
    shortUrl: {
        type: String,
        required: true
    }
});

const Link = mongoose.model('Link', LinksSchema);
module.exports = Link;